package com.example.denizen.coppermobile.model;

/**
 * Created by denizen on 2/15/18.
 */

public class SignInRequest {

    public String email;
    public String password;

    public SignInRequest() {

    }

    public SignInRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
