package com.example.denizen.coppermobile.viewModel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import com.example.denizen.coppermobile.BR;
import com.example.denizen.coppermobile.view.MainActivity;
import com.example.denizen.coppermobile.Utils;


/**
 * Created by denizen on 2/15/18.
 */

public class signInViewModel extends BaseObservable {

    private final static String DEFAULT_USERNAME = "admin";
    private final static String DEFAULT_PASSWORD = "password";

    public Context mContext;
    public String email = DEFAULT_USERNAME;
    public String password = DEFAULT_PASSWORD;
    public int statusCode;


    public signInViewModel() {
    }

    public signInViewModel(Context mContext) {
        this.mContext = mContext;
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    @Bindable
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
        notifyPropertyChanged(BR.statusCode);
    }

    public void onClickLogin(View view) {
        setLoginStatus();
        ((MainActivity) view.getContext()).processSignIn();
    }

    public void setLoginStatus() {
        if (email.equalsIgnoreCase(DEFAULT_USERNAME) && password.equals(DEFAULT_PASSWORD)) {
            setStatusCode(Utils.STATUS_OK);
        } else {
            setStatusCode(Utils.STATUS_ERR);
        }
    }

}
