package com.example.denizen.coppermobile.view;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.denizen.coppermobile.R;
import com.example.denizen.coppermobile.Utils;
import com.example.denizen.coppermobile.databinding.ActivityMainBinding;
import com.example.denizen.coppermobile.viewModel.signInViewModel;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setSignInViewModel(new signInViewModel());
    }


    public void processSignIn() {
        int status = binding.getSignInViewModel().getStatusCode();
        String message;

        if (status == Utils.STATUS_OK) {
            message = getString(R.string.success_message) + " (status=" + String.valueOf(status) + ")";
        } else {
            message = getString(R.string.failure_message) + " (status=" + String.valueOf(status) + ")";
        }


        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

}
