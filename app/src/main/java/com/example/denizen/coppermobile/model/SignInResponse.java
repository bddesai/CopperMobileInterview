package com.example.denizen.coppermobile.model;

/**
 * Created by denizen on 2/15/18.
 */

public class SignInResponse {
    public int statusCode;

    public SignInResponse(){}

    public SignInResponse(int statusCode){
        this.statusCode = statusCode;
    }
}
